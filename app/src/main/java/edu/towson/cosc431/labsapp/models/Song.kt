package edu.towson.cosc431.labsapp.models

data class Song (
    val name: String,
    val artist: String,
    val trackNum: Int,
    val isAwesome: Boolean
)